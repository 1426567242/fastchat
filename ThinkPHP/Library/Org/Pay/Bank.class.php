<?php
/**
 * Bank(银行汇款支付模块)
 */
namespace Org\Pay;
class Bank {
	
	public $config = array ();
	
	public function __construct($config = array()) {
		$this->config = $config;
	}
	
	public function setup() {
		$modules ['pay_name'] = L ( 'Bank_pay_name' );
		$modules ['pay_code'] = 'Bank';
		$modules ['pay_desc'] = L ( 'bank_desc' );
		$modules ['iscod'] = '0';
		$modules ['isonline'] = '0';
		$modules ['author'] = 'Windcms';
		$modules ['website'] = '';
		$modules ['version'] = '1.0.0';
		$modules ['config'] = array (
			array(
				'name' => 'opening_bank', 
				'type' => 'text', 
				'value' => ''
			),
			array(
				'name' => 'account_holder', 
				'type' => 'text', 
				'value' => ''
			),
			array(
				'name' => 'Card_No', 
				'type' => 'text', 
				'value' => ''
			),
		
		);
		return $modules;
	}
	
	public function get_code() {
		return;
	}
	public function respond() {
		return;
	}
}