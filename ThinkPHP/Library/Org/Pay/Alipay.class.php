<?php
/**
 * Alipay(支付宝支付模块)
 */
namespace Org\Pay;
class Alipay {
	public $config = array ();
	
	public function __construct($config = array()) {
		
	}
	
	/**
	 * 返回支付模块安装信息
	 * @access public
	 * @author ruby
	 * @version 20141114
	 */
	public function setup() {
		$modules ['pay_name'] = L ( 'alipay_pay_name' );
		$modules ['pay_code'] = 'Alipay';
		$modules ['pay_desc'] = L ( 'alipay_desc' );
		$modules ['iscod'] = '0';
		$modules ['isonline'] = '1';
		$modules ['author'] = 'Windcms';
		$modules ['website'] = 'http://www.alipay.com';
		$modules ['version'] = '1.0.0';
		$modules ['config'] = array (
			array (
				'name' => 'alipay_order', 
				'type' => 'text', 
				'value' => '',
				'desc'=>'将在支付宝支付页面显示订单名称,便于检测支付来源'),
			array (
				'name' => 'alipay_account', 
				'type' => 'text', 
				'value' => ''), 
			array (
				'name' => 'alipay_key',
				'type' => 'text',
				'value' => '' ),
			array (
				'name' => 'alipay_partner',
				'type' => 'text',
				'value' => '' ),
			array (
				'name' => 'alipay_pay_type',
				'type' => 'select',
				'value' => '',
				'option' => array (
					'1' => L ( 'alipay_pay_type_option1' ),
					'2' => L ( 'alipay_pay_type_option2' ),
					'3' => L ( 'alipay_pay_type_option3' ) )
			)
		);
		return $modules;
	}

	public function setconfig($config) {
		$this->config = $config;

		if ($this->config ['alipay_pay_type'] == 1){
			$this->config ['service'] = 'create_partner_trade_by_buyer'; //担保
		} elseif ($this->config ['alipay_pay_type'] == 3){
			$this->config ['service'] = 'create_direct_pay_by_user'; //即时
		} else {
			$this->config ['service'] = 'trade_create_by_buyer'; //标准
		}

		$this->config ['gateway_url'] = 'https://www.alipay.com/cooperate/gateway.do?';
		$this->config ['gateway_method'] = 'POST';

		$siteinfo = site_info ( 1 );
		$domain = str_replace ( 'http://', '', trim ( $siteinfo ['domain'], '/' ) );
		$this->config ['notify_url'] = $this->config ['return_url'] = $domain.'/index.php?m=Member&c=Finance&a=respond&code=Alipay';
	}
	
	/**
	 * 获取支付按钮代码
	 * @access public
	 * @author ruby
	 * @version 20141114
	 */
	public function get_code($btn_value = '', $btn_attr = '') {
		$parameter = array (
			'service' => $this->config ['service'], 
			'partner' => trim ( $this->config ['alipay_partner'] ), 
			'notify_url' => trim ( $this->config ['notify_url'] ), 
			'return_url' => trim ( $this->config ['return_url'] ),
			'_input_charset' => 'utf-8', 
            /* 商品信息 */
            'subject' => $this->config ['order_sn'], 
            'out_trade_no' => $this->config ['order_sn'], 
            'price' => $this->config ['order_amount'], 
            'body' => $this->config ['body'], 
            'quantity' => 1, 
            'payment_type' => 1,
            /* 物流参数 */
            'logistics_type' => 'EXPRESS', 
            'logistics_fee' => 0, 
            'logistics_payment' => 'BUYER_PAY_AFTER_RECEIVE', 
            //'agent'  => $this->config['agent'], 
			/* 买卖双方信息 */
			'seller_email' => trim ( $this->config ['alipay_account'])
		);
		ksort ( $parameter );
		reset ( $parameter );
		$param = '';
		$sign = '';
		foreach ( $parameter as $key => $val ) {
			$param .= "$key=" . urlencode ( $val ) . "&";
			$sign .= "$key=$val&";
		}
		$param = substr ( $param, 0, - 1 );
		$sign = substr ( $sign, 0, - 1 ) . $this->config ['alipay_key'];
		$button = '<span><button type="button" onclick="window.open(\'' . $this->config ['gateway_url'] . $param . '&sign=' . MD5 ( $sign ) . '&sign_type=MD5\');" '.$btn_attr.'>'.$btn_value.'</button></span>';
		return $button;
	}
	
	/**
	 * 回调 支付后同步更改交易订单的状态（TODO：待测试）
	 * @access public
	 * @author ruby
	 * @version 20141114
	 */
	public function respond() {
		if (! empty ( $_POST )) {
			foreach ( $_POST as $key => $data ) {
				$_GET [$key] = $data;
			}
		}
		
		$seller_email = rawurldecode ( $_GET ['seller_email'] );
		$order_sn = trim ( $_GET ['out_trade_no'] );
		/* 检查数字签名是否正确 */
		ksort ( $_GET );
		reset ( $_GET );
		$sign = '';
		foreach ( $_GET as $key => $val ) {
			if ($key != 'sign' && $key != 'sign_type' && $key != 'code' && $key != 'm' && $key != 'c' && $key != 'a') {
				$sign .= "$key=$val&";
			}
		}
		$sign = substr ( $sign, 0, - 1 ) . $this->config ['alipay_key'];
		if (md5 ( $sign ) != $_GET ['sign']) {
			return false;
		}
		if ($_GET ['trade_status'] == 'WAIT_SELLER_SEND_GOODS' || $_GET ['trade_status'] == 'WAIT_BUYER_CONFIRM_GOODS' || $_GET ['trade_status'] == 'WAIT_BUYER_PAY') {
			/* 改变订单状态 进行中*/
			order_pay_status ( $order_sn, 'progress' );
			return true;
		} elseif ($_GET ['trade_status'] == 'TRADE_FINISHED') {
			/* 改变订单状态 */
			order_pay_status ( $order_sn, 'succ' );
			return true;
		} elseif ($_GET ['trade_status'] == 'TRADE_SUCCESS') {
			/* 改变订单状态 即时交易成功*/
			order_pay_status ( $order_sn, 'succ' );
			return true;
		} else {
			return false;
		}
	}
}