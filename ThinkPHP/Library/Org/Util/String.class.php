<?php
/**
 * 字符串工具类
 * @author mcyami
 * @version 20150319
 */
namespace Org\Util;
class String {
	
	/**
	 * 生成UUID 单机使用
	 * UUID是指在一台机器上生成的数字，它保证对在同一时空中的所有机器都是唯一的。通常平台会提供生成的API。
	 * @access public
	 * @return string
	 */
	static public function uuid() {
		$charid = md5 ( uniqid ( mt_rand (), true ) );
		$hyphen = chr ( 45 ); // "-"
		$uuid = chr ( 123 ) . // "{"
substr ( $charid, 0, 8 ) . $hyphen . substr ( $charid, 8, 4 ) . $hyphen . substr ( $charid, 12, 4 ) . $hyphen . substr ( $charid, 16, 4 ) . $hyphen . substr ( $charid, 20, 12 ) . chr ( 125 ); // "}"
		return $uuid;
	}
	
	/**
	 * 生成Guid主键(Key Generator注册机)
	 * @return Boolean
	 */
	static public function keyGen() {
		return str_replace ( '-', '', substr ( String::uuid (), 1, - 1 ) );
	}
	
	/**
	 * 产生随机字串,可用来自动生成密码,默认长度6位 字母和数字混合 支持中文
	 * @param string $len 长度
	 * @param string $type 字串类型{0:大小写字母混合;1:数字;2:大写字母;3:小写字母;4:中文;其他:大小字母加数字混合}
	 * @param string $addChars 额外字符
	 * @return string
	 */
	static public function randString($len = 6, $type = 5, $addChars = '') {
		$str = '';
		switch ($type) {
			case 0 :
				$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' . $addChars;
				break;
			case 1 :
				$chars = str_repeat ( '0123456789', 3 );
				break;
			case 2 :
				$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . $addChars;
				break;
			case 3 :
				$chars = 'abcdefghijklmnopqrstuvwxyz' . $addChars;
				break;
			case 4 :
				$chars = "们以我到他会作时要动国产的一是工就年阶义发成部民可出能方进在了不和有大这主中人上为来分生对于学下级地个用同行面说种过命度革而多子后自社加小机也经力线本电高量长党得实家定深法表着水理化争现所二起政三好十战无农使性前等反体合斗路图把结第里正新开论之物从当两些还天资事队批点育重其思与间内去因件日利相由压员气业代全组数果期导平各基或月毛然如应形想制心样干都向变关问比展那它最及外没看治提五解系林者米群头意只明四道马认次文通但条较克又公孔领军流入接席位情运器并飞原油放立题质指建区验活众很教决特此常石强极土少已根共直团统式转别造切九你取西持总料连任志观调七么山程百报更见必真保热委手改管处己将修支识病象几先老光专什六型具示复安带每东增则完风回南广劳轮科北打积车计给节做务被整联步类集号列温装即毫知轴研单色坚据速防史拉世设达尔场织历花受求传口断况采精金界品判参层止边清至万确究书术状厂须离再目海交权且儿青才证低越际八试规斯近注办布门铁需走议县兵固除般引齿千胜细影济白格效置推空配刀叶率述今选养德话查差半敌始片施响收华觉备名红续均药标记难存测士身紧液派准斤角降维板许破述技消底床田势端感往神便贺村构照容非搞亚磨族火段算适讲按值美态黄易彪服早班麦削信排台声该击素张密害侯草何树肥继右属市严径螺检左页抗苏显苦英快称坏移约巴材省黑武培著河帝仅针怎植京助升王眼她抓含苗副杂普谈围食射源例致酸旧却充足短划剂宣环落首尺波承粉践府鱼随考刻靠够满夫失包住促枝局菌杆周护岩师举曲春元超负砂封换太模贫减阳扬江析亩木言球朝医校古呢稻宋听唯输滑站另卫字鼓刚写刘微略范供阿块某功套友限项余倒卷创律雨让骨远帮初皮播优占死毒圈伟季训控激找叫云互跟裂粮粒母练塞钢顶策双留误础吸阻故寸盾晚丝女散焊功株亲院冷彻弹错散商视艺灭版烈零室轻血倍缺厘泵察绝富城冲喷壤简否柱李望盘磁雄似困巩益洲脱投送奴侧润盖挥距触星松送获兴独官混纪依未突架宽冬章湿偏纹吃执阀矿寨责熟稳夺硬价努翻奇甲预职评读背协损棉侵灰虽矛厚罗泥辟告卵箱掌氧恩爱停曾溶营终纲孟钱待尽俄缩沙退陈讨奋械载胞幼哪剥迫旋征槽倒握担仍呀鲜吧卡粗介钻逐弱脚怕盐末阴丰雾冠丙街莱贝辐肠付吉渗瑞惊顿挤秒悬姆烂森糖圣凹陶词迟蚕亿矩康遵牧遭幅园腔订香肉弟屋敏恢忘编印蜂急拿扩伤飞露核缘游振操央伍域甚迅辉异序免纸夜乡久隶缸夹念兰映沟乙吗儒杀汽磷艰晶插埃燃欢铁补咱芽永瓦倾阵碳演威附牙芽永瓦斜灌欧献顺猪洋腐请透司危括脉宜笑若尾束壮暴企菜穗楚汉愈绿拖牛份染既秋遍锻玉夏疗尖殖井费州访吹荣铜沿替滚客召旱悟刺脑措贯藏敢令隙炉壳硫煤迎铸粘探临薄旬善福纵择礼愿伏残雷延烟句纯渐耕跑泽慢栽鲁赤繁境潮横掉锥希池败船假亮谓托伙哲怀割摆贡呈劲财仪沉炼麻罪祖息车穿货销齐鼠抽画饲龙库守筑房歌寒喜哥洗蚀废纳腹乎录镜妇恶脂庄擦险赞钟摇典柄辩竹谷卖乱虚桥奥伯赶垂途额壁网截野遗静谋弄挂课镇妄盛耐援扎虑键归符庆聚绕摩忙舞遇索顾胶羊湖钉仁音迹碎伸灯避泛亡答勇频皇柳哈揭甘诺概宪浓岛袭谁洪谢炮浇斑讯懂灵蛋闭孩释乳巨徒私银伊景坦累匀霉杜乐勒隔弯绩招绍胡呼痛峰零柴簧午跳居尚丁秦稍追梁折耗碱殊岗挖氏刃剧堆赫荷胸衡勤膜篇登驻案刊秧缓凸役剪川雪链渔啦脸户洛孢勃盟买杨宗焦赛旗滤硅炭股坐蒸凝竟陷枪黎救冒暗洞犯筒您宋弧爆谬涂味津臂障褐陆啊健尊豆拔莫抵桑坡缝警挑污冰柬嘴啥饭塑寄赵喊垫丹渡耳刨虎笔稀昆浪萨茶滴浅拥穴覆伦娘吨浸袖珠雌妈紫戏塔锤震岁貌洁剖牢锋疑霸闪埔猛诉刷狠忽灾闹乔唐漏闻沈熔氯荒茎男凡抢像浆旁玻亦忠唱蒙予纷捕锁尤乘乌智淡允叛畜俘摸锈扫毕璃宝芯爷鉴秘净蒋钙肩腾枯抛轨堂拌爸循诱祝励肯酒绳穷塘燥泡袋朗喂铝软渠颗惯贸粪综墙趋彼届墨碍启逆卸航衣孙龄岭骗休借" . $addChars;
				break;
			default :
				// 默认去掉了容易混淆的字符oOLl和数字01，要添加请使用addChars参数
				$chars = 'ABCDEFGHIJKMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789' . $addChars;
				break;
		}
		// 位数过长重复字符串一定次数
		if ($len > 10) {
			$chars = $type == 1 ? str_repeat ( $chars, $len ) : str_repeat ( $chars, 5 );
		}
		if ($type != 4) {
			$chars = str_shuffle ( $chars );
			$str = substr ( $chars, 0, $len );
		} else {
			// 中文随机字
			for($i = 0; $i < $len; $i ++) {
				$str .= self::msubstr ( $chars, floor ( mt_rand ( 0, mb_strlen ( $chars, 'utf-8' ) - 1 ) ), 1, 'utf-8', false );
			}
		}
		return $str;
	}
	
	/**
	 * 获取一定范围内的随机数字,位数不足补零
	 * @param integer $min 最小值
	 * @param integer $max 最大值
	 * @return string
	 */
	static public function randNumber($min, $max) {
		return sprintf ( "%0" . strlen ( $max ) . "d", mt_rand ( $min, $max ) );
	}
	
	/**
	 * 生成一定数量的随机数，并且不重复
	 * @param integer $number 数量
	 * @param string $len 长度
	 * @param string $type 字串类型{0:字母;1:数字;其它:混合}
	 * @return array
	 */
	static public function buildCountRand($number, $length = 4, $mode = 1) {
		if ($mode == 1 && $length < strlen ( $number )) {
			// 不足以生成一定数量的不重复数字
			return false;
		}
		$rand = array ();
		for($i = 0; $i < $number; $i ++) {
			$rand [] = self::randString ( $length, $mode );
		}
		$unqiue = array_unique ( $rand );
		if (count ( $unqiue ) == count ( $rand )) {
			return $rand;
		}
		$count = count ( $rand ) - count ( $unqiue );
		for($i = 0; $i < $count * 3; $i ++) {
			$rand [] = self::randString ( $length, $mode );
		}
		$rand = array_slice ( array_unique ( $rand ), 0, $number );
		return $rand;
	}
	
	/**
	 * 带格式生成随机字符,支持批量生成,但可能存在重复
	 * @param string $format 字符格式{# 表示数字 * 表示字母和数字 $ 表示字母}
	 * @param integer $number 生成数量
	 * @return string | array
	 */
	static public function buildFormatRand($format, $number = 1) {
		$str = array ();
		$length = strlen ( $format );
		for($j = 0; $j < $number; $j ++) {
			$strtemp = '';
			for($i = 0; $i < $length; $i ++) {
				$char = substr ( $format, $i, 1 );
				switch ($char) {
					case "*" : // 字母和数字混合
						$strtemp .= String::randString ( 1 );
						break;
					case "#" : // 数字
						$strtemp .= String::randString ( 1, 1 );
						break;
					case "$" : // 大写字母
						$strtemp .= String::randString ( 1, 2 );
						break;
					default : // 其他格式均不转换
						$strtemp .= $char;
						break;
				}
			}
			$str [] = $strtemp;
		}
		return $number == 1 ? $strtemp : $str;
	}
	
	/**
	 * 返回经addslashes处理过的字符串或数组
	 * @param $string 需要处理的字符串或数组
	 * @return mixed
	 */
	static public function newAddslashes($string) {
		if (! is_array ( $string )) {
			return addslashes ( $string );
		}
		foreach ( $string as $key => $val ) {
			$string [$key] = String::newAddslashes ( $val );
		}
		return $string;
	}
	
	/**
	 * 返回经stripslashes处理过的字符串或数组
	 * @param $string 需要处理的字符串或数组
	 * @return mixed
	 */
	static public function newStripslashes($string) {
		if (! is_array ( $string )) {
			return stripslashes ( $string );
		}
		foreach ( $string as $key => $val ) {
			$string [$key] = String::newStripslashes ( $val );
		}
		return $string;
	}
	
	/**
	 * 返回经htmlspecialchars处理过的字符串或数组,把一些预定义的字符转换为 HTML实体
	 * @param mixed $string 需要处理的字符串或数组
	 * @return mixed
	 */
	static public function newHtmlSpecialChars($string) {
		$encoding = 'utf-8';
		if (! is_array ( $string )) {
			return htmlspecialchars ( $string, ENT_QUOTES, $encoding );
		}
		foreach ( $string as $key => $val ) {
			$string [$key] = String::newHtmlSpecialChars ( $val );
		}
		return $string;
	}
	
	/**
	 * 返回经html_entity_decode处理过的字符串或数组,把 HTML实体转换为字符
	 * @param string $string 需要处理的字符串或数组
	 */
	static public function newHtmlEntityDecode($string) {
		$encoding = 'utf-8';
		if (! is_array ( $string )) {
			return html_entity_decode ( $string, ENT_QUOTES, $encoding );
		}
		foreach ( $string as $key => $val ) {
			$string [$key] = String::newHtmlEntityDecode ( $val );
		}
		return $string;
	}
	
	/**
	 * 转换所有html标记为实体字符,且中文为乱码
	 * @param string $string
	 */
	static public function newHtmlentities($string) {
		$encoding = 'utf-8';
		if (! is_array ( $string )) {
			return htmlentities ( $string, ENT_QUOTES, $encoding );
		}
		foreach ( $string as $key => $val ) {
			$string [$key] = String::newHtmlentities ( $val );
		}
		return $string;
	}
	
	/**
	 * 字符串安全过滤
	 * @param string $string
	 */
	static public function filterSafe($string) {
		$string = str_replace ( '%20', '', $string );
		$string = str_replace ( '%27', '', $string );
		$string = str_replace ( '%2527', '', $string );
		$string = str_replace ( '*', '', $string );
		$string = str_replace ( '"', '&quot;', $string );
		$string = str_replace ( "'", '', $string );
		$string = str_replace ( '"', '', $string );
		$string = str_replace ( ';', '', $string );
		$string = str_replace ( '<', '&lt;', $string );
		$string = str_replace ( '>', '&gt;', $string );
		$string = str_replace ( "{", '', $string );
		$string = str_replace ( '}', '', $string );
		$string = str_replace ( '\\', '', $string );
		return $string;
	}
	
	/**
	 * xss过滤
	 * @param $string
	 * @return string
	 */
	static public function filterXss($string) {
		$string = preg_replace ( '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S', '', $string );
		$parm1 = Array ('javascript', 'vbscript', 'expression', 'applet', 'meta', 'xml', 'blink', 'link', 'script', 'embed', 'object', 'iframe', 'frame', 'frameset', 'ilayer', 'layer', 'bgsound', 'title', 'base' );
		$parm2 = Array ('onabort', 'onactivate', 'onafterprint', 'onafterupdate', 'onbeforeactivate', 'onbeforecopy', 'onbeforecut', 'onbeforedeactivate', 'onbeforeeditfocus', 'onbeforepaste', 'onbeforeprint', 'onbeforeunload', 'onbeforeupdate', 'onblur', 'onbounce', 'oncellchange', 'onchange', 'onclick', 'oncontextmenu', 'oncontrolselect', 'oncopy', 'oncut', 'ondataavailable', 'ondatasetchanged', 'ondatasetcomplete', 'ondblclick', 'ondeactivate', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'onerror', 'onerrorupdate', 'onfilterchange', 'onfinish', 'onfocus', 'onfocusin', 'onfocusout', 'onhelp', 'onkeydown', 'onkeypress', 'onkeyup', 'onlayoutcomplete', 'onload', 'onlosecapture', 'onmousedown', 'onmouseenter', 'onmouseleave', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onmove', 'onmoveend', 'onmovestart', 'onpaste', 'onpropertychange', 'onreadystatechange', 'onreset', 'onresize', 'onresizeend', 'onresizestart', 'onrowenter', 'onrowexit', 'onrowsdelete', 'onrowsinserted', 'onscroll', 'onselect', 'onselectionchange', 'onselectstart', 'onstart', 'onstop', 'onsubmit', 'onunload' );
		$parm = array_merge ( $parm1, $parm2 );
		for($i = 0; $i < sizeof ( $parm ); $i ++) {
			$pattern = '/';
			for($j = 0; $j < strlen ( $parm [$i] ); $j ++) {
				if ($j > 0) {
					$pattern .= '(';
					$pattern .= '(&#[x|X]0([9][a][b]);?)?';
					$pattern .= '|(&#0([9][10][13]);?)?';
					$pattern .= ')?';
				}
				$pattern .= $parm [$i] [$j];
			}
			$pattern .= '/i';
			$string = preg_replace ( $pattern, ' ', $string );
		}
		return $string;
	}
	
	/**
	 * 过滤ASCII码从0-28的控制字符
	 * @return String
	 */
	static public function filterUnsafeControlChars($str) {
		$rule = '/[' . chr ( 1 ) . '-' . chr ( 8 ) . chr ( 11 ) . '-' . chr ( 12 ) . chr ( 14 ) . '-' . chr ( 31 ) . ']*/';
		return str_replace ( chr ( 0 ), '', preg_replace ( $rule, '', $str ) );
	}
	
	/**
	 * 格式化文本域内容
	 * @param $string 文本域内容
	 * @return string
	 */
	static public function formatTextarea($string) {
		$string = nl2br ( str_replace ( ' ', '&nbsp;', $string ) );
		return $string;
	}
	
	/**
	 * 将文本格式成适合js输出的字符串
	 * @param string $string 需要处理的字符串
	 * @param intval $isjs 是否执行字符串格式化，默认为执行
	 * @return string 处理后的字符串
	 */
	static public function formatJs($string, $isjs = 1) {
		$string = addslashes ( str_replace ( array ("\r", "\n", "\t" ), array ('', '', '' ), $string ) );
		return $isjs ? 'document.write("' . $string . '");' : $string;
	}
	
	/**
	 * 转义 javascript 代码标记
	 * @param $str
	 * @return mixed
	 */
	static public function formatScript($str) {
		if (is_array ( $str )) {
			foreach ( $str as $key => $val ) {
				$str [$key] = String::formatScript ( $val );
			}
		} else {
			$str = preg_replace ( '/\<([\/]?)script([^\>]*?)\>/si', '&lt;\\1script\\2&gt;', $str );
			$str = preg_replace ( '/\<([\/]?)iframe([^\>]*?)\>/si', '&lt;\\1iframe\\2&gt;', $str );
			$str = preg_replace ( '/\<([\/]?)frame([^\>]*?)\>/si', '&lt;\\1frame\\2&gt;', $str );
			$str = str_replace ( 'javascript:', 'javascript：', $str );
		}
		return $str;
	}
	
	/**
	 * 判断字符串是否为utf8编码，英文和半角字符返回ture
	 * @param $string
	 * @return bool
	 */
	static public function isUtf8($string) {
		return preg_match ( '%^(?:
					[\x09\x0A\x0D\x20-\x7E] # ASCII
					| [\xC2-\xDF][\x80-\xBF] # non-overlong 2-byte
					| \xE0[\xA0-\xBF][\x80-\xBF] # excluding overlongs
					| [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2} # straight 3-byte
					| \xED[\x80-\x9F][\x80-\xBF] # excluding surrogates
					| \xF0[\x90-\xBF][\x80-\xBF]{2} # planes 1-3
					| [\xF1-\xF3][\x80-\xBF]{3} # planes 4-15
					| \xF4[\x80-\x8F][\x80-\xBF]{2} # plane 16
					)*$%xs', $string );
	}
	
	/**
	 * 自动转换字符集 支持数组转换
	 * @param string/array $string 要转换的字符或者数组
	 * @param string $from 原来的编码
	 * @param string $to 要转换为的编码
	 */
	static public function autoCharset($string, $from = 'gbk', $to = 'utf-8') {
		$from = strtoupper ( $from ) == 'UTF8' ? 'utf-8' : $from;
		$to = strtoupper ( $to ) == 'UTF8' ? 'utf-8' : $to;
		if (strtoupper ( $from ) === strtoupper ( $to ) || empty ( $string ) || (is_scalar ( $string ) && ! is_string ( $string ))) {
			// 如果编码相同或者非字符串标量则不转换
			return $string;
		}
		if (is_string ( $string )) {
			if (function_exists ( 'mb_convert_encoding' )) {
				return mb_convert_encoding ( $string, $to, $from );
			} elseif (function_exists ( 'iconv' )) {
				return iconv ( $from, $to, $string );
			} else {
				return $string;
			}
		} elseif (is_array ( $string )) {
			foreach ( $string as $key => $val ) {
				$_key = self::autoCharset ( $key, $from, $to );
				$string [$_key] = self::autoCharset ( $val, $from, $to );
				if ($key != $_key) {
					unset ( $string [$key] );
				}
			}
			return $string;
		} else {
			return $string;
		}
	}
	
	/**
	 * 字符串截取，支持中文和其他编码(指定开始位置,截取长度,一个汉字相对于一个字符)
	 * @static
	 * @access public
	 * @param string $str 需要转换的字符串
	 * @param string $length 截取长度
	 * @param string $start 开始位置
	 * @param string $charset 编码格式
	 * @param string $suffix 截断显示字符
	 * @return string
	 */
	static public function msubstr($str, $length, $start = 0, $charset = "utf-8", $suffix = true) {
		if (function_exists ( "mb_substr" )) {
			$slice = mb_substr ( $str, $start, $length, $charset );
		} elseif (function_exists ( 'iconv_substr' )) {
			$slice = iconv_substr ( $str, $start, $length, $charset );
		} else {
			$re ['utf-8'] = "/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|[\xe0-\xef][\x80-\xbf]{2}|[\xf0-\xff][\x80-\xbf]{3}/";
			$re ['gb2312'] = "/[\x01-\x7f]|[\xb0-\xf7][\xa0-\xfe]/";
			$re ['gbk'] = "/[\x01-\x7f]|[\x81-\xfe][\x40-\xfe]/";
			$re ['big5'] = "/[\x01-\x7f]|[\x81-\xfe]([\x40-\x7e]|\xa1-\xfe])/";
			preg_match_all ( $re [$charset], $str, $match );
			$slice = join ( "", array_slice ( $match [0], $start, $length ) );
		}
		return $suffix ? $slice . '...' : $slice;
	}
	
	/**
	 * 将字符串转换为数组
	 * @param string $data 字符串
	 * @return array 返回数组格式，如果，data为空，则返回空数组
	 */
	static public function string2array($data) {
		if ($data == '') {
			return array ();
		}
		if (is_array ( $data )) {
			return $data;
		}
		if (strpos ( $data, 'array' ) !== false && strpos ( $data, 'array' ) === 0) {
			@eval ( "\$array = $data;" );
			return $array;
		}
		return unserialize ( ($data) ); //unserialize ( new_stripslashes ( $data ) );
	}
	
	/**
	 * 将数组转换为字符串
	 * @param array $data 数组
	 * @param bool $isformdata 如果为0，则不使用new_stripslashes处理，可选参数，默认为1
	 * @return string 返回字符串，如果，data为空，则返回空
	 */
	static public function array2string($data, $isformdata = 1) {
		if ($data == '') {
			return '';
		}
		if ($isformdata) {
			$data = ($data); //new_stripslashes ( $data );
		}
		return serialize ( $data );
	}
	
	/**
	 * 对用户的密码进行加密
	 * @param $password
	 * @param $encrypt 传入加密串，在修改密码时做认证
	 * @return array/password
	 */
	static public function pwd($password, $encrypt = '') {
		$pwd = array ();
		$pwd ['encrypt'] = $encrypt ? $encrypt : String::randString ();
		$pwd ['password'] = md5 ( md5 ( trim ( $password ) ) . $pwd ['encrypt'] );
		return $encrypt ? $pwd ['password'] : $pwd;
	}
	
	/**
	 * 数据签名认证
	 * @param array $data
	 * @return string
	 */
	static public function authSign($data) {
		// 数据类型检测
		if (! is_array ( $data )) {
			$data = ( array ) $data;
		}
		ksort ( $data ); // 排序
		$code = http_build_query ( $data ); // url编码并生成query字符串
		$sign = sha1 ( $code ); // 生成签名
		return $sign;
	}
	
	/**
	 * 转换字节数为其他单位
	 * @param	string	$filesize	字节大小
	 * @return	string	返回大小
	 */
	static public function sizeCount($filesize) {
		if ($filesize >= 1073741824) {
			$filesize = round ( $filesize / 1073741824 * 100 ) / 100 . ' GB';
		} elseif ($filesize >= 1048576) {
			$filesize = round ( $filesize / 1048576 * 100 ) / 100 . ' MB';
		} elseif ($filesize >= 1024) {
			$filesize = round ( $filesize / 1024 * 100 ) / 100 . ' KB';
		} else {
			$filesize = $filesize . ' Bytes';
		}
		return $filesize;
	}
	
	/**
	 * 加密解密
	 * @param string $string	需要加密或者解密的值
	 * @param string $operation	DECODE:解密  ENCODE:加密
	 * @param string $key	加密因子(SSO_KEY)
	 * @param int $expiry	有效期
	 */
	static public function authCode($string, $operation = 'ENCODE', $key = '', $expiry = 0) {
		$ckey_length = 4; // 随机密钥长度 取值 0-32;
		// 加入随机密钥，可以令密文无任何规律，即便是原文和密钥完全相同，加密结果也会每次不同，增大破解难度。
		// 取值越大，密文变动规律越大，密文变化 = 16 的 $ckey_length 次方
		// 当此值为 0 时，则不产生随机密钥
		$key = md5 ( $key ? $key : 'abcdefg' ); //md5加密KEY,生成一个32位的key值
		$keya = md5 ( substr ( $key, 0, 16 ) ); //取key的1到16位,然后再进行md5加密,生成一个新的32位key
		$keyb = md5 ( substr ( $key, 16, 16 ) ); //取key的17到32位,然后再进行md5加密,生成一个新的32位key
		if ($ckey_length) {
			if ($operation == 'DECODE') {
				//解密的时候取值,$string的前4位
				$keyc = substr ( $string, 0, $ckey_length );
			} else {
				//加密的时候的取值,加密后时间戳的后4位
				$keyc = substr ( md5 ( microtime () ), - $ckey_length );
			}
		} else {
			$keyc = '';
		}
		//生成的$cryptkey的示例:189160757c3aff50a5171639465a8136 c62d3c157e9afa608c4f87d1f68f4158
		$cryptkey = $keya . md5 ( $keya . $keyc );
		$key_length = strlen ( $cryptkey ); //64
		if ($operation == 'DECODE') {
			//执行解密
			$string = base64_decode ( substr ( $string, $ckey_length ) );
		} else {
			//加密
			//sprintf('%010d',$xxx) %010d的意思就是生成10位,如果d达不到长度就用0补充
			//生成的string例子:0000000000449e590b7dc46168abcd  10位+16位+$string的长度
			$string = sprintf ( '%010d', $expiry ? $expiry + time () : 0 ) . substr ( md5 ( $string . $keyb ), 0, 16 ) . $string;
		}
		$string_length = strlen ( $string );
		$result = '';
		$box = range ( 0, 255 );
		$rndkey = array ();
		for($i = 0; $i <= 255; $i ++) {
			$rndkey [$i] = ord ( $cryptkey [$i % $key_length] );
		}
		//把$box的数组打乱
		for($j = $i = 0; $i < 256; $i ++) {
			$j = ($j + $box [$i] + $rndkey [$i]) % 256;
			$tmp = $box [$i];
			$box [$i] = $box [$j];
			$box [$j] = $tmp;
		}
		for($a = $j = $i = 0; $i < $string_length; $i ++) {
			$a = ($a + 1) % 256;
			$j = ($j + $box [$a]) % 256;
			$tmp = $box [$a];
			$box [$a] = $box [$j];
			$box [$j] = $tmp;
			$result .= chr ( ord ( $string [$i] ) ^ ($box [($box [$a] + $box [$j]) % 256]) );
		}
		if ($operation == 'DECODE') {
			if ((substr ( $result, 0, 10 ) == 0 || substr ( $result, 0, 10 ) - time () > 0) && substr ( $result, 10, 16 ) == substr ( md5 ( substr ( $result, 26 ) . $keyb ), 0, 16 )) {
				return substr ( $result, 26 );
			} else {
				return '';
			}
		} else {
			return $keyc . str_replace ( '=', '', base64_encode ( $result ) );
		}
	}

}