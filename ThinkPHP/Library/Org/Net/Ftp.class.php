<?php
/**
 * FTP类
 * Frame/Library/Org/Util/Ftp.class.php
 *
 * @author windcms
 * @copyright http://www.windcms.com
 * @version 20140324 mcyami@gmail.com
 */
namespace Org\Net;
class Ftp {
	 // FTP连接资源
	private $link;
	//FTP连接时间
	public $link_time;
	private $host; // ftp地址
	private $username;	// 用户名
	private $password;	// 密码
	private $path; // 根目录
	private $port; // 端口
	private $pasv; // 被动模式
	private $ssl; // ssl连接方式
	private $timeout; // 超时时间
	//错误代码
	private $err_code = 0;
	//传送模式{文本模式:FTP_ASCII, 二进制模式:FTP_BINARY}
	public $mode = FTP_BINARY;
	public $status;
	/**
	 * 初始化构造方法,连接FTP服务器
	 * 1:成功;2:无法连接ftp;3:用户错误;
	 * @param unknown_type $host
	 * @param unknown_type $username
	 * @param unknown_type $password
	 * @param unknown_type $path
	 * @param unknown_type $port
	 * @param unknown_type $pasv
	 * @param unknown_type $ssl
	 * @param unknown_type $timeout
	 */
	public function __construct($host = "", $username = "", $password = "", $path = "", $port = "21", $pasv = false, $ssl = false, $timeout = 30) {
		if ($host) {
			$this->host = $host;
		}
		if ($username) {
			$this->username = $username;
		}
		if ($password) {
			$this->password = $password;
		}
		if ($path) {
			$this->path = $path;
		}
		if ($port) {
			$this->port = $port;
		}
		if ($pasv) {
			$this->pasv = $pasv;
		}
		if ($ssl) {
			$this->ssl = $ssl;
		}
		if ($timeout){
			$this->timeout = $timeout;
		}
	}
	
	/**
	 * 连接FTP服务器
	 * @access public
	 */
	public function connect(){
		$start = time();
		if ($this->ssl) {
			if (!$this->link = @ftp_ssl_connect($this->host, $this->port, $this->timeout)) {
				$this->err_code = 1;
				return false;
			}
		} else {
			if (!$this->link = @ftp_connect($this->host, $this->port, $this->timeout)) {
				$this->err_code = 1;
				return false;
			}
		}
		if (@ftp_login($this->link, $this->username, $this->password)) {
			if ($this->pasv) {
				ftp_pasv($this->link, true);
			}
			$this->link_time = time()-$start;
		   return true;
		} else {
			$this->err_code = 8;
			return false;
		}
		register_shutdown_function(array(&$this,'bye'));
		
	}
	
	/**
	 * 切换目录
	 * @param string $dir
	 */
	public function cd($dir) {
		return ftp_chdir ( $this->link, $dir );
	}
	
	/**
	 * 返回当前路径
	 */
	public function pwd() {
		return ftp_pwd ( $this->link );
	}
	
	/**
	 * 创建目录
	 * @param string $directory 目录名
	 */
	public function mkdir($directory) {
		return ftp_mkdir ( $this->link, $directory );
	}
	
	/**
	 * 删除目录
	 * @param string $directory 目录名
	 */
	public function rmdir($directory) {
		return ftp_rmdir ( $this->link, $directory );
	}
	
	/**
	 * 上传文件
	 * @param unknown_type $localFile
	 * @param unknown_type $remoteFile
	 */
	public function put($localFile, $remoteFile = '') {
		if ($remoteFile == '') {
			$remoteFile = end ( explode ( '/', $localFile ) );
		}
		$res = ftp_nb_put ( $this->ftpR, $remoteFile, $localFile, FTP_BINARY );
		while ( $res == FTP_MOREDATA ) {
			$res = ftp_nb_continue ( $this->link );
		}
		if ($res == FTP_FINISHED) {
			return true;
		} elseif ($res == FTP_FAILED) {
			return false;
		}
	}
	
	/**
	 * 下载文件
	 * @param unknown_type $remoteFile
	 * @param unknown_type $localFile
	 */
	public function get($remoteFile, $localFile = '') {
		if ($localFile == '') {
			$localFile = end ( explode ( '/', $remoteFile ) );
		}
		if (ftp_get ( $this->ftpR, $localFile, $remoteFile, FTP_BINARY )) {
			$flag = true;
		} else {
			$flag = false;
		}
		return $flag;
	}
	
	/**
	 * 返回文件大小
	 * @param unknown_type $file
	 */
	public function size($file) {
		return ftp_size ( $this->link, $file );
	}
	
	/**
	 * 文件是否存在
	 * @param unknown_type $file
	 */
	public function isFile($file) {
		if ($this->size ( $file ) >= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 文件时间
	 * @param unknown_type $file
	 */
	public function fileTime($file) {
		return ftp_mdtm ( $this->link, $file );
	}
	
	/**
	 * 删除文件
	 * @param unknown_type $file
	 */
	public function unlink($file) {
		return ftp_delete ( $this->link, $file );
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $dir
	 */
	public function nlist($dir = '/service/resource/') {
		return ftp_nlist ( $this->link, $dir );
	}
	
	/**
	 * 关闭FTP连接
	 */
	public function bye() {
		return @ftp_close ( $this->link );
	}
	
	/**
	 * 获取错误信息
	 */
	public function get_error() {
		if (!$this->err_code) {
			return false;
		}
		$err_msg = array(
			'1' => 'server_can_not_connect',
			'2' => 'not_connect_to_server',
			'3' => 'can_not_delete_non_empty_folder',
			'4' => 'can_not_delete_file',
			'5' => 'can_not_get_file_list',
			'6' => 'can_not_change_the_current_directory_on_the_server',
			'7' => 'can_not_upload_files',
			'8' => 'ftp_user_is_error',
		);
		return $err_msg[$this->err_code];
	}
	
}
?>