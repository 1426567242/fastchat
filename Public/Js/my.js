// 删除提示
function confirm_delete(url, msg) {
	msg = msg ? msg : "确认要删除此信息吗?";
	art.dialog.confirm(msg, function() {
		ajaxget(url);
	});
}
//
function ajaxget(url) {
	$.get(url, function(data) {
		var data = eval("("+data+")");
		art.dialog.tips(data.info, 2);
		setTimeout(function() {
			window.location.reload();
		}, 1000);
	});
}
// 全/反选
function selectall(name) {
	if ($("#check_box").prop("checked")) {
		$("input[name='" + name + "']").each(function() {
			$(this).parent("span").attr('class','checked');
			$(this).attr('checked','checked');
		});
	} else {
		$("input[name='" + name + "']").each(function() {
			$(this).parent("span").attr('class','');
			$(this).removeAttr('checked');
		});
	}
}