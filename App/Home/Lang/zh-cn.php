<?php
return array(
	'text_save_to_desktop' => '保存到桌面',
	
	'text_user_login' => '用户登录',
	'text_user_register' => '用户注册',
	'text_user_profile' => '用户信息',
	'text_password_keep_empty' => '密码不改则留空',

	/*	成功	*/
	'success_publish_content' => '内容发布成功',

	/*	错误	*/
	'error_chatroom_not_exists' => '聊天室不存在',
	'error_chatroom_closed' => '聊天室已关闭',
	'error_not_allow_visitor' => '游客不允许聊天',
	'error_chatcontent_empty' => '内容不能为空',
);