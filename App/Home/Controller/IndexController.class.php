<?php
/**
 * 前台首页管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-10
 */
namespace Home\Controller;
use Common\Controller\HomeBaseController;
class IndexController extends HomeBaseController {
	
	/**
	 * 初始化
	 * @see Common\Controller.HomeBaseController::_initialize()
	 */
	public function _initialize() {
		parent::_initialize ();
	}
	
	/**
	 * 聊天室首页
	 */
	public function index() {
		$id = I ( 'get.id' );
		$roomid = empty ( $id ) ? 1 : $id;
		$roomInfo = D ( 'ChatRoom' )->getRoom ( 'id', $roomid ); //根据ID获取聊天室信息
		if (empty ( $roomInfo )) { //聊天室不存在
			$this->error ( L ( 'error_chatroom_not_exists' ) );
		}
		if (empty ( $roomInfo ['status'] )) { //聊天室关闭
			$this->error ( L ( 'error_chatroom_closed' ) );
		}
		//TODO 判断当前用户是否已被踢出
		//TODO 所有成员
		//TODO 初始化直播内容
		$user_session = session ( 'user_auth' );
		if ($user_session) {
			$user = D ( 'User' )->getUser ( 'uid', $user_session ['uid'] );
		}
		$this->assign ( $roomInfo );
		$this->assign ( 'offline_time', C ( 'OFFLINE_TIME' ) );
		$this->assign ( 'roomid', $roomid );
		$this->assign ( 'user', $user );
		$this->display ();
	}
	
	/**
	 * 初始化在线成员
	 */
	public function setUserOnline() {
		$roomid = I ( 'get.roomid' );
		if (empty ( $roomid ) || ! is_numeric ( $roomid )) {
			echo json_encode ( '' );
			exit ();
		}
		if (D ( 'UserOnline' )->getUser ( $this->_User ['user_online_id'], $roomid )) {
			//更新在线用户信息
			D ( 'UserOnline' )->editUserOnline ( $this->_User ['user_online_id'], $roomid, C ( 'OFFLINE_TIME' ) );
		} else {
			D ( 'UserOnline' )->addUserOnline ( $roomid, $this->_User, C ( 'OFFLINE_TIME' ) ); //新增在线用户
		}
		$users = D ( 'UserOnline' )->getUserOnline ( $roomid, C ( 'OFFLINE_TIME' ) ); //获取在线用户列表
		$str = '';
		foreach ( $users as $k => $v ) {
			if (! empty ( $v ['user_online_id'] ) && ! empty ( $v ['user_online_name'] )) {
				if ($v ['user_online_id'] == $this->_User ['user_online_id']) { //当前在线用户
					$str .= '<li><font color="red">' . $v ['user_online_name'] . '</font></li>';
				} else {
					$str .= '<li>' . $v ['user_online_name'] . '</li>';
				}
			}
		}
		$str .= "<script>$('#usertotal').html(" . count ( $users ) . ");</script>";
		echo json_encode ( $str );
		exit ();
	}
	
	/**
	 * 发布聊天内容
	 */
	public function publishContent() {
		if (C ( 'VISITOR_ACTIVE' ) == 0) { //判断是否开启允许游客聊天功能
			echo json_encode ( array ('code' => 0, 'msg' => L ( 'error_not_allow_visitor' ) ) );
			exit ();
		}
		//TODO判断是否能发送图片、表情
		$content = I ( 'post.content' );
		if (empty ( $content )) { //发布内容不能为空
			echo json_encode ( array ('code' => 0, 'msg' => L ( 'error_chatcontent_empty' ) ) );
			exit ();
		}
		//TODO判断用户是否被禁言 status:0
		$status = 1;
		//TODO 敏感词过滤
		$roomid = I ( 'post.roomid' );
		$result = D ( 'ChatContent' )->addContent ( $this->_User ['user_online_id'], $this->_User ['user_online_name'], $roomid, $content, $status );
		if ($result) {
			$return ['code'] = 1;
			$return ['msg'] = L ( 'success_publish_content' );
			$return ['content'] = $this->dealContent ( $result );
			echo json_encode ( $return );
			exit ();
		} else {
			echo json_encode ( array ('code' => 0, 'msg' => $result ) );
			exit ();
		}
	}
	
	/**
	 * 内容处理成制定格式
	 * @param unknown_type $id
	 */
	public function dealContent($id) {
		$content = D ( 'ChatContent' )->getContent ( 'id', $id );
		if ($content) {
			$str = '<li class="current-user">';
			$user = D ( 'User' )->getUser ( 'uid', $content ['chat_content_userid'] );
			if ($user && ! empty ( $user ['avatar'] )) {
				$str .= '<img width="30" height="30" src="' . $user ['avatar'] . '" />';
			} else {
				$str .= '<img width="30" height="30" src="./Public/Img/front/avatar.png" />';
			}
			$str .= '<div class="bubble"><a class="user-name" href="javascript:;">' . $content ['chat_content_username'] . '</a> &nbsp;&nbsp;<font class="time" size="1"> ' . date ( 'Y-m-d H:i:s', $content ['createtime'] ) . '</font>';
			$str .= '<p class="message"> ' . $content ['chat_content'] . '</p></div></li>';
			return $str;
		}
		return '';
	}
	
	/**
	 * 实时请求聊天内容
	 */
	public function getChatContent($roomid) {
		$roomid = empty ( $roomid ) ? I ( 'get.roomid' ) : $roomid;
		$list = D ( 'ChatContent' )->getContentList ( $roomid );
		krsort ( $list );
		$str = '';
		if ($list) {
			foreach ( $list as $k => $v ) {
				if ($v ['chat_content_userid'] == $this->_User ['user_online_id']) {
					$str .= '<li class="current-user">';
				} else {
					$str .= '<li>';
				}
				$user = D ( 'User' )->getUser ( 'uid', $v ['chat_content_userid'] );
				if ($user && ! empty ( $user ['avatar'] )) {
					$str .= '<img width="30" height="30" src="' . $user ['avatar'] . '" />';
				} else {
					$str .= '<img width="30" height="30" src="./Public/Img/front/avatar.png" />';
				}
				$str .= '<div class="bubble"><a class="user-name" href="javascript:;">' . $v ['chat_content_username'] . '</a> &nbsp;&nbsp;<font class="time" size="1"> ' . date ( 'Y-m-d H:i:s', $v ['createtime'] ) . '</font>';
				$str .= '<p class="message"> ' . $v ['chat_content'] . '</p></div></li>';
			}
		}
		echo json_encode ( $str );
		exit ();
	}
}