<?php
/**
 * 用户角色管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-27
 */
namespace Admin\Controller;
class RoleController extends BaseController {
	
	public function _initialize() {
		parent::_initialize ();
	
	}
	
	/**
	 * 角色列表页面
	 */
	public function index() {
		$where = array ();
		$title = I ( 'post.title' );
		if ($title) {
			$where ['title'] = array ('like', "%{$title}%" );
		}
		$count = D ( 'AuthGroup' )->getCount ( $where );
		$limit = $this->paging ( $count );
		$lists = D ( 'AuthGroup' )->getGroup ( $where, 'id asc', $limit );
		$this->assign ( 'list', $lists );
		$this->display ();
	}
	
	/**
	 * 编辑用户角色
	 */
	public function editGroup() {
		$id = I ( 'get.id' );
		if (empty ( $id )) {
			$this->error ( L ( 'error_illegal_data' ) ); //非法数据
		}
		$groups = D ( 'AuthGroup' )->getGroupById ( $id );
		$this->assign ( $groups );
		$this->display ( 'edit' );
	}
	
	/**
	 * 新增用户角色
	 */
	public function createGroup() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$_POST ['rules'] = '';
			if (false === D ( 'AuthGroup' )->create ()) {
				$this->error ( D ( 'AuthGroup' )->getError () );
			} else {
				if (D ( 'AuthGroup' )->add ()) {
					$this->success ( L ( 'success_insert' ), U ( 'Role/index' ) );
				} else {
					$this->error ( L ( 'error_insert' ) );
				}
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) ); //非法操作
		}
	}
	
	/**
	 * 更新用户组信息
	 */
	public function updateGroup() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			if ($_POST ['rules']) {
				sort ( $_POST ['rules'] );
				$_POST ['rules'] = implode ( ',', $_POST ['rules'] ); //将数组转换为字符串
			}
			if (false === D ( 'AuthGroup' )->create ()) {
				$this->error ( D ( 'AuthGroup' )->getError () );
			} else {
				if (D ( 'AuthGroup' )->save ()) {
					$this->success ( L ( 'success_edit' ), U ( 'Role/index' ) );
				} else {
					$this->error ( L ( 'error_edit' ) );
				}
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) ); //非法操作
		}
	}
	
	/**
	 * 更改用户组启禁用状态
	 */
	public function setStatus() {
		$getdata = I ( 'get.' );
		if (empty ( $getdata ['id'] )) {
			$this->error ( L ( 'error_illegal_data' ) );
		}
		$getdata ['status'] = empty ( $getdata ['status'] ) ? 1 : 0;
		$result = D ( 'AuthGroup' )->save ( $getdata );
		if ($result) {
			$this->success ( L ( 'success_edit' ), U ( 'index' ) );
		} else {
			$this->error ( L ( 'error_edit' ) );
		}
	}
	
	/**
	 * 访问授权页面
	 */
	public function access() {
		$this->updteRules ();
		$id = empty ( $_GET ['id'] ) ? 1 : $_GET ['id'];
		//所有角色组列表
		$authGroup = D ( 'AuthGroup' )->getGroupField ( 'id,title,rules', array ('group_type' => 1, 'status' => 1 ) );
		$nodelists = $this->returnNodes (); //所有节点
		$map = array ('type' => 2, 'status' => 1 );
		$mainrules = D ( 'AuthRule' )->getRuleField ( 'name,id', $map ); //一级节点
		$where = array ('type' => 1, 'status' => 1 );
		$childrules = D ( 'AuthRule' )->getRuleField ( 'name,id', $where ); //二级节点		
		$this->assign ( 'authgroup', $authGroup );
		$this->assign ( 'mainrules', $mainrules );
		$this->assign ( 'nodelist', $nodelists );
		$this->assign ( 'childrule', $childrules );
		$this->assign ( 'group', $authGroup [( int ) $id] ); //制定角色组信息，角色组ID、租名、规则
		$this->display ();
	}
	
	/**
	 * 后台节点配置的url作为规则存入auth_rule
	 */
	public function updteRules() {
		$nodes = $this->returnNodes ( false ); //获取节点
		$where = array ('type' => array ('in', '1,2' ) ); //1:url 2:主菜单
		$rules = D ( 'AuthRule' )->getRule ( $where ); //获取所有符合条件的节点
		$data = array (); //定义空数组用于保存需要插入和更新的节点
		foreach ( $nodes as $value ) {
			$temp ['name'] = $value ['url'];
			$temp ['module'] = $value ['module'];
			$temp ['title'] = $value ['title'];
			if ($value ['parentid'] > 0) {
				$temp ['type'] = 1;
			} else {
				$temp ['type'] = 2;
			}
			$temp ['status'] = 1;
			//去除重复项
			$data [strtolower ( $temp ['name'] . $temp ['module'] . $temp ['type'] )] = $temp;
		}
		$update = array (); //保存需要更新的节点
		$ids = array (); //保存需要删除的节点
		foreach ( $rules as $k => $v ) {
			$key = strtolower ( $v ['name'] . $v ['module'] . $v ['type'] );
			if (isset ( $data [$key] )) { //如果数据库中的规则与配置的节点匹配，说明是需要更新的节点
				$data [$key] ['id'] = $v ['id']; //为需要更新的节点补充ID值
				$update [] = $data [$key];
				unset ( $data [$key] );
				unset ( $rules [$k] );
				unset ( $v ['condition'] );
				$diff [$v ['id']] = $v;
			} elseif ($v ['status'] == 1) {
				$ids [] = $v ['id'];
			}
		}
		if (count ( $update )) {
			foreach ( $update as $up => $row ) {
				if ($row != $diff [$row ['id']]) {
					$map ['id'] = $row ['id'];
					D ( 'AuthRule' )->editRule ( $map, $row );
				}
			}
		}
		if (count ( $ids )) {
			$condition ['id'] = array ('in', implode ( ',', $ids ) );
			D ( 'AuthRule' )->editRule ( $condition, array ('status' => 0 ) );
		}
		if (count ( $data )) {
			D ( 'AuthRule' )->addAll ( array_values ( $data ) );
		}
		if (D ( 'AuthRule' )->getDbError ()) {
			return false;
		} else {
			return true;
		}
	}
	
	/**
	 * 返回后台节点数据
	 * @param boolen $tree 是否返回多位数组结构，为false返回一维数组
	 */
	public function returnNodes($tree = true) {
		$treeNodes = array ();
		if ($tree && ! empty ( $treeNodes [( int ) $tree] )) {
			return $treeNodes [$tree];
		}
		if (( int ) $tree) {
			$field = 'id,parentid,title,url,description,status,module';
			$list = D ( 'Menu' )->getList ( $field );
			foreach ( $list as $k => $v ) {
				if (stripos ( $v ['url'], $v ['module'] ) !== 0) {
					$list [$k] ['url'] = $v ['module'] . '/' . $v ['url'];
				}
			}
			$treeList = new \Org\Util\Tree ();
			$nodes = $treeList->list_to_tree ( $list, 'id', 'parentid', 'operator', 0 );
			foreach ( $nodes as $key => $value ) {
				if (! empty ( $value ['operator'] )) {
					$nodes [$key] ['child'] = $value ['operator'];
					unset ( $nodes [$key] ['operator'] );
				}
			}
		} else {
			$field = 'title,url,module,parentid,description';
			$nodes = D ( 'Menu' )->getList ( $field );
			foreach ( $nodes as $k => $v ) {
				if (stripos ( $v ['url'], $v ['module'] ) !== 0) {
					$nodes [$k] ['url'] = $v ['module'] . '/' . $v ['url'];
				}
			}
		}
		$treeNodes [( int ) $tree] = $nodes;
		return $nodes;
	}
	
	/**
	 * 批量删除用户角色组
	 */
	public function delGroup() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$ids = I ( 'post.ids' );
			if ($ids) {
				foreach ( $ids as $id ) {
					D ( 'AuthGroup' )->delete ( $id );
				}
				$this->success ( L ( 'success_delete' ) );
			}
		}
		$this->error ( L ( 'error_illegal_operation' ) );
	}
	
	/**
	 * 单个删除用户角色组
	 */
	public function deleteGroup() {
		$id = I ( 'get.id' );
		if (empty ( $id )) {
			echo json_encode ( array ('info' => L ( 'error_select_the_data' ) ) );
			exit ();
		}
		if (false === D ( 'AuthGroup' )->delete ( $id )) {
			$return = array ('info' => L ( 'error_delete' ) );
		} else {
			$return = array ('info' => L ( 'success_delete' ) );
		}
		echo json_encode ( $return );
		exit ();
	}
}