<?php
/**
 * 后台公共控制器,不经过权限认证
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: 小陈先生 <945146147@qq.com>
 * @date: 2015-5-4
 */
namespace Admin\Controller;
class PublicController extends \Think\Controller {
	
	public function _initialize() {
	
	}
	
	/**
	 * 后台登陆(登录页和登陆处理)
	 * @access public
	 */
	public function login() {
		if (D ( 'User' )->checkLogin ()) {
			$this->redirect ( 'Index/index' );
		} else {
			/* 读取数据库中的配置 */
			$config = S ( 'DB_CONFIG_DATA' );
			if (! $config) {
				$config = D ( 'Config' )->getList ();
				S ( 'DB_CONFIG_DATA', $config );
			}
			// 添加配置
			C ( $config );
			$this->display ();
		}
	}
	
	/**
	 * 登录操作处理
	 * @access public
	 */
	public function doLogin() {
		if (IS_POST) {
			/* 检测验证码 */
			if (C ( 'ADMIN_VERIFY' )) {
				$captcha = trim ( I ( 'post.captcha' ) );
				if (! D ( 'User' )->checkVerify ( $captcha )) {
					$this->error ( L ( 'error_captcha' ), HTTP_REFERER );
				}
			}
			/* 检测用户名和密码 */
			$username = I ( 'post.username', '' );
			$password = I ( 'post.password', '' );
			if (empty ( $username ) || empty ( $password )) {
				$this->error ( L ( 'error_name_pass_empty' ), HTTP_REFERER );
			}
			/* 检测登录失败次数 */
			$times = $this->_checkLoginTimes ( $username, 1 );
			if ($times === false) {
				$this->error ( L ( 'error_failed_login_times', array ('minute' => C ( 'FAILED_LOCK_TIME' ) ) ), HTTP_REFERER );
			}
			// 用户登录
			$uid = $this->_login ( $username, $password, 1 );
			if ($uid <= 0) {
				$this->error ( L ( 'error_login_' . $uid ), HTTP_REFERER );
			}
			// 登录成功后删除用户登录失败记录
			D ( 'LoginTimes' )->delLoginRecord ( $username, 1 );
			$this->autoLogin ( $uid, $username );
			$this->success ( L ( 'success_login' ), U ( 'Index/index' ) );
		}
	}
	
	/**
	 * 检测登录失败次数
	 * @param string $username
	 * @param int $isadmin
	 */
	private function _checkLoginTimes($username, $isadmin) {
		$maxfailedtimes = ( int ) C ( 'MAX_LOGIN_FAILED_TIMES' );
		$locktime = ( int ) C ( 'FAILED_LOCK_TIME' );
		$rtime = D ( 'LoginTimes' )->getLoginTimes ( $username, $isadmin );
		if ($rtime && ($rtime ['times'] >= $maxfailedtimes)) {
			$minute = $locktime - floor ( (NOW_TIME - $rtime ['login_time']) / 60 );
			if ($minute > 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 用户登录逻辑过程处理
	 * @param string $username
	 * @param string $password
	 * @param int $isadmin
	 * @param int $type
	 * @access private
	 */
	private function _login($username, $password, $isadmin, $type = 1) {
		$where = array ();
		switch ($type) {
			case 1 :
				$field = 'username';
				break;
			case 2 :
				$field = 'email';
				break;
			case 3 :
				$field = 'mobile';
				break;
			case 4 :
				$field = 'uid';
				break;
			default :
				return 0; // 参数错误
		}
		$user = D ( 'User' )->getUser ( $field, $username ); // 获取用户数据
		if (is_array ( $user )) {
			if (! $user ['status']) {
				return - 2; // 用户被禁用
			}
			/* 验证用户密码 */
			if (D ( 'User' )->checkPassword ( $user ['uid'], $password )) {
				D ( 'User' )->editLoginInfo ( $user ['uid'] ); // 更新用户登录信息
				return $user ['uid']; // 登录成功，返回用户ID
			} else {
				// 密码错误，添加一次登录失败次数
				if (D ( 'LoginTimes' )->getLoginTimes ( $username, $isadmin )) {
					D ( 'LoginTimes' )->editLoginTimes ( $username, $isadmin );
				} else {
					D ( 'LoginTimes' )->addLoginTimes ( $username, $isadmin );
				}
				return - 3;
			}
		} else {
			return - 1; // 用户不存在
		}
	}
	
	/**
	 * 将用户登录信息存入session
	 * @param int $uid
	 * @param string $username
	 * @param string $expire
	 * @access public
	 */
	protected function autoLogin($uid, $username, $expire = 86400) {
		$auth ['uid'] = $uid;
		$auth ['uname'] = $username;
		$auth ['expire'] = $expire;
		session ( 'user_auth', $auth );
		session ( 'user_auth_sign', \Org\Util\String::authSign ( $auth ) );
	}
	
	/**
	 * 退出
	 * @access public
	 */
	public function logout() {
		if (D ( 'User' )->checkLogin ()) {
			session ( null );
			session ( 'user_auth', NULL );
			session ( 'user_auth_sign', NULL );
			session ( '[destroy]' );
			$this->success ( '退出成功！', U ( 'login' ) );
		} else {
			$this->redirect ( 'login' );
		}
	}
	
	/**
	 * 验证码输出
	 * @access public
	 */
	public function verify() {
		$verify = new \Think\Verify ();
		$verify->entry ( 1 );
	}

}