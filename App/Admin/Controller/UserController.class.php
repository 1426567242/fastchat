<?php
/**
 * 用户管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-27
 */
namespace Admin\Controller;
class UserController extends BaseController {
	
	public function _initialize() {
		parent::_initialize ();
		//获取所有用户组
		$authGroup = D ( 'AuthGroup' )->getGroupField ( 'id,title,module', array ('group_type' => 1, 'status' => 1 ) );
		$this->assign ( 'authGroup', $authGroup );
	}
	
	/**
	 * 用户列表
	 */
	public function index() {
		$where = array ();
		$username = I ( 'post.username' );
		if ($username) {
			$where ['username'] = array ('like', '%' . $username . '%' );
		}
		$where ['uid'] = array ('neq', 1 ); //不显示超管账户
		$count = D ( 'User' )->getUserCount ( $where );
		$limit = $this->paging ( $count );
		$list = D ( 'User' )->getUserList ( $where, 'uid asc', $limit );
		foreach ( $list as $k => $v ) {
			if ($v ['reg_time']) {
				$list [$k] ['reg_time'] = date ( 'Y-m-d H:i:s', $v ['reg_time'] );
			}
			if ($v ['last_login_time']) {
				$list [$k] ['last_login_time'] = date ( 'Y-m-d H:i:s', $v ['last_login_time'] );
			}
		}
		$this->assign ( 'list', $list );
		$this->display ();
	}
	
	/**
	 * 编辑用户 - 页面显示
	 */
	public function editUser() {
		$uid = I ( 'get.uid' );
		if (empty ( $uid )) {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
		$user = D ( 'User' )->getUser ( 'uid', $uid );
		$group_id = D ( 'AuthGroupAccess' )->getGrouopById ( $uid );
		$this->assign ( 'group_id', $group_id );
		$this->assign ( $user );
		$this->display ( 'edit' );
	}
	
	/**
	 * 新增用户具体操作 - 数据表插入用户
	 */
	public function addUser() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			$info ['realname'] = $postdata ['realname'];
			$info ['qq'] = $postdata ['qq'];
			if (C ( 'UC_OPEN' )) { //开启了Ucenter
				$uid = uc_user_register ( $postdata ['username'], $postdata ['password'], $postdata ['email'] );
				if ($uid <= 0) {
					$this->error ( L ( 'error_uc_register_' . $uid ) );
				}
				$info ['ucid'] = $uid;
			}
			$userid = D ( 'User' )->addUser ( $postdata ['username'], $postdata ['password'], $postdata ['email'], $postdata ['mobile'], $info );
			if ($userid > 0) {
				if ($postdata ['group_id']) {
					D ( 'AuthGroupAccess' )->addGroupAccess ( $userid, $postdata ['group_id'] );
				}
				$this->success ( L ( 'success_insert' ), U ( 'index' ) );
			} else {
				$this->error ( L ( 'error_register_' . $userid ) );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
	
	/**
	 * 更新用户信息
	 */
	public function updateUser() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			$user = D ( 'User' )->getUser ( 'uid', $postdata ['uid'] );
			if ($postdata ['password']) {
				if (C ( 'UC_OPEN' )) {
					$status = uc_user_edit ( $user ['username'], 0, $postdata ['password'], $postdata ['email'] );
					if ($status <= 0) {
						$this->error ( L ( 'error_uc_edit_' . $status ) );
					}
				}
				$data ['password'] = \Org\Util\String::pwd ( $postdata ['password'], $user ['encrypt'] );
			}
			if ($postdata ['realname'] != $user ['realname']) { //修改了真实姓名时
				$data ['realname'] = $postdata ['realname'];
			}
			if ($postdata ['email'] != $user ['email']) { //修改了邮箱时
				$data ['email'] = $postdata ['email'];
			}
			if ($postdata ['mobile'] != $user ['mobile']) { //修改了手机号时
				$data ['mobile'] = $postdata ['mobile'];
			}
			if ($postdata ['qq'] != $user ['qq']) { //修改了QQ时
				$data ['qq'] = $postdata ['qq'];
			}
			if ($postdata ['group_id'] != D ( 'AuthGroupAccess' )->getGrouopById ( $postdata ['uid'] )) {
				D ( 'AuthGroupAccess' )->addGroupAccess ( $postdata ['uid'], $postdata ['group_id'] );
			}
			if ($data) {
				$result = D ( 'User' )->editUser ( $postdata ['uid'], '', $data );
				if (false === $result) {
					$this->error ( D ( 'User' )->getError () );
				} else {
					$this->success ( L ( 'success_edit' ) );
				}
			} else {
				$this->error ( L ( 'error_uc_edit_0' ) );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
	
	/**
	 * 更改用户状态
	 */
	public function setStatus() {
		$getdata = I ( 'get.' );
		if (empty ( $getdata ['uid'] )) {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
		$getdata ['status'] = empty ( $getdata ['status'] ) ? 1 : 0;
		$result = D ( 'User' )->save ( $getdata );
		if ($result) {
			$this->success ( L ( 'success_edit' ), U ( 'index' ) );
		} else {
			$this->error ( L ( 'error_edit' ) );
		}
	}
	
	/**
	 * 批量删除用户
	 */
	public function delUser() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$ids = I ( 'post.ids' );
			if ($ids) {
				if (C ( 'UC_OPEN' )) {
					foreach ( $ids as $uid ) {
						$user = D ( 'User' )->getUser ( 'uid', $uid );
						uc_user_delete ( $user ['ucid'] );
					}
				}
				D ( 'User' )->deleteUser ( $ids );
				D ( 'AuthGroupAccess' )->deleteGroupAccess ( $ids );
				$this->success ( L ( 'success_delete' ) );
			}
		}
		$this->error ( L ( 'error_illegal_operation' ) );
	}
	
	/**
	 * 单个删除用户
	 */
	public function deleteUser() {
		$uid = I ( 'get.uid' );
		if (empty ( $uid )) {
			echo json_encode ( array ('info' => L ( 'error_select_the_data' ) ) );
			exit ();
		}
		if (C ( 'UC_OPEN' )) {
			$user = D ( 'User' )->getUser ( 'uid', $uid );
			$result = uc_user_delete ( $user ['ucid'] );
			if ($result) {
				$res = D ( 'User' )->deleteUser ( $uid );
				D ( 'AuthGroupAccess' )->deleteGroupAccess ( $uid );
			} else {
				$return = array ('info' => L ( 'error_delete' ) );
				echo json_encode ( $return );
				exit ();
			}
		} else {
			$res = D ( 'User' )->deleteUser ( $uid );
			D ( 'AuthGroupAccess' )->deleteGroupAccess ( $uid );
		}
		if ($res == false) {
			$return = array ('info' => L ( 'error_delete' ) );
		} else {
			$return = array ('info' => L ( 'success_delete' ) );
		}
		echo json_encode ( $return );
		exit ();
	}
}