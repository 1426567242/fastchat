<?php
/**
 * 分类管理控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-9
 */
namespace Admin\Controller;
class ClassifyController extends BaseController {
	
	/**
	 * 初始化
	 * @see Admin\Controller.BaseController::_initialize()
	 */
	public function _initialize() {
		parent::_initialize ();
	}
	
	/**
	 * 分类列表
	 */
	public function index() {
		$where = array ();
		$classify_name = I ( 'post.classify_name' );
		if ($classify_name) {
			$where ['classify_name'] = array ('like', '%' . $classify_name . '%' );
			$this->assign ( 'classify_name', $classify_name );
		}
		$count = D ( 'Classify' )->getCount ( $where );
		$limit = $this->paging ( $count );
		$list = D ( 'Classify' )->getList ( $where, 'sort_order desc,id asc', $limit );
		$this->assign ( 'list', $list );
		$this->display ();
	}
}