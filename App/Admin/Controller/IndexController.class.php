<?php
/**
 * 后台首页
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author 小陈先生 <945146147@qq.com>
 * @version 20150430 
 */
namespace Admin\Controller;
class IndexController extends BaseController {
	
	/**
	 * 初始化
	 * @access public
	 */
	public function _initialize() {
		parent::_initialize ();
	}
	
	/**
	 * 后台首页
	 * @access public
	 */
	public function index() {
		if (UID) {
			//用户统计
			$userCount = D ( 'User' )->count ();
			$this->assign ( 'user_count', $userCount );
			//系统信息
			$sysInfo = array ('VERSION' => VERSION, 'SERVER' => PHP_OS, 'RUNNING' => $_SERVER ['SERVER_SOFTWARE'], 'MYSQL_VERSION' => mysql_get_server_info (), 'UPLOAD_LIMIT' => '2M' );
			$this->assign ( $sysInfo );
			$this->display ();
		} else {
			$this->redirect ( 'Public/login' );
		}
		\Think\Hook::listen ();
	}
	
	/**
	 * 个人资料管理 - 显示页面
	 */
	public function profile() {
		if (UID) {
			$users = D ( 'User' )->getUser ( 'uid', UID );
			$users ['last_login_time'] = date ( 'Y-m-d H:i:s', $users ['last_login_time'] );
			$this->assign ( $users );
		}
		$this->display ();
	}
	
	/**
	 * 修改个人资料
	 */
	public function editProfile() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			if (empty ( $postdata ['email'] ) && empty ( $postdata ['mobile'] ) && empty ( $postdata ['realname'] )) {
				$this->error ( L ( 'error_email_mobil_realname_empty' ) );
			}
			$result = D ( 'User' )->editUser ( UID, '', $postdata );
			if ($result === false) {
				$error = D ( 'User' )->getError ();
				if (is_numeric ( $error )) {
					$this->error ( L ( 'error_check_' . $error ) );
				} else {
					$this->error ( $error );
				}
			}
			$this->success ( L ( 'success_editprofile' ) );
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
	
	/**
	 * 修改密码
	 */
	public function editPassword() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			//旧密码和新密码都不能为空
			if (empty ( $postdata ['oldpwd'] ) || empty ( $postdata ['newpwd'] )) {
				$this->error ( L ( 'error_oldpwd_newpwd_empty' ) );
			}
			//确认密码为空或与新密码不一致
			if (empty ( $postdata ['repwd'] ) || $postdata ['newpwd'] != $postdata ['repwd']) {
				$this->error ( L ( 'error_repwd_empty' ) );
			}
			$data = \Org\Util\String::pwd ( $postdata ['newpwd'] );
			$result = D ( 'User' )->editUser ( UID, $postdata ['oldpwd'], $data );
			if ($result === false) {
				$this->error ( D ( 'User' )->getError () );
			}
			$this->success ( L ( 'success_editpwd' ) );
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}
	
	/**
	 * 锁屏 - 页面显示
	 */
	public function lock() {
		$this->display ();
	}
	
	/**
	 * 解锁
	 */
	public function unlock() {
		if (IS_POST && isset ( $_POST ['dosubmit'] )) {
			$postdata = I ( 'post.' );
			if (empty ( $postdata ['password'] )) {
				$this->error ( L ( 'error_enter_the_password_to_unlock' ) );
			}
			$result = D ( 'User' )->checkPassword ( UID, $postdata ['password'] );
			if ($result) {
				$this->redirect ( 'Index/index' );
			} else {
				$this->error ( L ( 'error_login_-3' ) );
			}
		} else {
			$this->error ( L ( 'error_illegal_operation' ) );
		}
	}

}