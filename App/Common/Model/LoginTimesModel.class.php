<?php
/**
 * 登录次数模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-18
 */
namespace Common\Model;
class LoginTimesModel extends \Think\Model {
	
	protected $_validate = array ();
	/*	数据自动完成	*/
	protected $_auto = array (
		array ('ip', 'get_client_ip', self::MODEL_INSERT, 'function' ), 
		array ('login_time', NOW_TIME ) 
	);
	
	public function _initialize() {
	
	}
	
	/**
	 * 添加登录失败次数
	 * @param string $username
	 * @param int $isadmin
	 * @access public
	 */
	public function addLoginTimes($username, $isadmin) {
		$data ['username'] = $username;
		$data ['is_admin'] = $isadmin;
		$data ['times'] = 1;
		if ($this->create ( $data )) {
			return $this->add ();
		} else {
			return $this->getError ();
		}
	}
	
	/**
	 * 修改登录失败次数，+1
	 * @param unknown_type $username
	 * @param unknown_type $isadmin
	 * @access public
	 */
	public function editLoginTimes($username, $isadmin) {
		$where ['username'] = $username;
		$where ['is_admin'] = $isadmin;
		$this->where ( $where )->setInc ( 'times', 1 );
	}
	
	/**
	 * 根据用户和角色类型返回用户登录失败信息
	 * @param string $username
	 * @param int $isadmin
	 * @access public
	 */
	public function getLoginTimes($username, $isadmin) {
		$where ['username'] = $username;
		$where ['is_admin'] = $isadmin;
		$rtime = $this->where ( $where )->find ();
		if (is_array ( $rtime )) {
			return $rtime;
		} else {
			return false;
		}
	}
	
	/**
	 * 删除登录记录
	 * @param string $username
	 * @param int $isadmin
	 * @access public
	 */
	public function delLoginRecord($username, $isadmin) {
		$where ['username'] = $username;
		$where ['is_admin'] = $isadmin;
		$this->where ( $where )->delete ();
	}
}