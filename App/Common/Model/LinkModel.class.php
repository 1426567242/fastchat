<?php
/**
 * 友情链接模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-22
 */
namespace Common\Model;
class LinkModel extends \Think\Model {
	
	protected $_validate = array (
		array ('link_name', 'require', '链接名称不能为空', self::MUST_VALIDATE, 'regex', self::MODEL_INSERT ), 
		array ('link_name', '', '链接名称已存在', self::VALUE_VALIDATE, 'unique', self::MODEL_INSERT ) 
	);
	protected $_auto = array ();
	
	public function _initialize() {
	
	}
	
	/**
	 * 添加友情链接
	 * @param string $link_name
	 * @param string $link_url
	 * @param string $link_logo
	 * @param int $status
	 */
	public function addLink($link_name, $link_url, $link_logo, $status) {
		$data = array('link_name'=>$link_name,'linkurl'=>$link_url,'link_logo'=>$link_logo,'status'=>$status);
		if ($this->create ( $data )) {
			$id = $this->add ();
			return $id ? $id : 0; //大于0-添加成功
		} else {
			return $this->getError (); 
		}
	}
	
	/**
	 * 修改友情链接
	 * @param int $id
	 * @param array $data
	 */
	public function editLink($id, $data){
		if (empty ( $id )){
			$this->error = L ( 'error_not_exists' );
			return false;
		}
		$data = $this->create ( $data );
		if ($data) {
			return $this->where ( array ('id' => $id ) )->save ( $data );
		}
		return false;
	}
	
	/**
	 * 根据条件获取链接数目
	 * @param array $where
	 */
	public function getCount($where = array()) {
		return $this->where ( $where )->count ();
	}
	
	/**
	 * 获取友情链接列表
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getList($where = array(), $order = 'id asc', $limit = '') {
		return $this->where ( $where )->order ( $order )->limit ( $limit )->select ();
	}
}