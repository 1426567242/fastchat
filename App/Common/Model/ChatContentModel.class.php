<?php
/**
 * 聊天内容模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-17
 */
namespace Common\Model;
class ChatContentModel extends \Think\Model {
	
	protected $_validate = array ();
	protected $_auto = array (
		array ('createtime', NOW_TIME, self::MODEL_INSERT ) 
	);
	
	public function _initialize() {
	
	}
	
	/**
	 * 新增聊天内容
	 * @param string $chat_content_userid
	 * @param string $chat_content_username
	 * @param int $roomid
	 * @param string $chat_content
	 * @param int $status
	 */
	public function addContent($chat_content_userid, $chat_content_username, $roomid, $chat_content, $status) {
		if (empty ( $chat_content_userid ) || empty ( $roomid ) || empty ( $chat_content )) {
			$this->error = L ( 'error_parameter' );
			return false;
		}
		$data = array ('chat_content_userid' => $chat_content_userid, 'chat_content_username' => $chat_content_username, 'roomid' => $roomid, 'chat_content' => $chat_content, 'status' => $status );
		if ($this->create ( $data )) {
			$contentid = $this->add ();
			return $contentid ? $contentid : 0;
		} else {
			return $this->getError ();
		}
	}
	
	/**
	 * 获取聊天内容列表
	 * @param int $roomid
	 */
	public function getContentList($roomid) {
		if (empty ( $roomid )) {
			$this->error = L ( 'error_illegal_data' );
			return faslse;
		}
		$where ['roomid'] = $roomid;
		return $this->where ( $where )->order ( 'createtime desc,id asc' )->limit ( 20 )->select ();
	}
	
	/**
	 * 根据相应条件获取指定内容
	 * @param string $field
	 * @param string $value
	 */
	public function getContent($field = 'id', $value) {
		$field_array = array ('id', 'chat_content_userid', 'roomid' );
		if (in_array ( $field, $field_array )) {
			$where [$field] = $value;
			$content = $this->where ( $where )->find ();
			if (is_array ( $content )) {
				return $content;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}