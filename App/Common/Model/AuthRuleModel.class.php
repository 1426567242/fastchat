<?php
/**
 * 权限节点模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-28
 */
namespace Common\Model;
class AuthRuleModel extends \Think\Model {
	
	public function _initialize() {
	
	}
	
	/**
	 * 根据条件获取节点列表
	 * @param array $where
	 * @param string $order
	 */
	public function getRule($where = array(), $order = 'id asc') {
		return $this->where ( $where )->order ( $order )->select ();
	}
	
	/**
	 * 更新节点信息
	 * @param array $where
	 * @param array $data
	 */
	public function editRule($where = array(), $data = array()) {
		return $this->where ( $where )->save ( $data );
	}
	
	/**
	 * 根据条件获取权限节点指定字段信息
	 * @param string $field
	 * @param array $where
	 */
	public function getRuleField($field, $where = array()){
		return $this->where ( $where )->getField ( $field );
	}
}