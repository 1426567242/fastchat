<?php
/**
 * 地区管理模型
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-22
 */
namespace Common\Model;
class AreaModel extends \Think\Model {
	
	protected $_validate = array (
		array ('region_name', 'require', '地区名称不能为空' ) 
	);
	
	protected $_auto = array ();
	
	public function _initialize() {
	
	}
	
	/**
	 * 更新地区信息
	 * @param int $id
	 * @param string $region_name
	 */
	public function editArea($id, $data) {
		$data = $this->create ( $data );
		if ($data) {
			return $this->where( array ('id' => $id ) )->save ( $data );
		}
		return false;
	}
	
	/**
	 * 根据条件获取地区列表
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getList($where = array(), $order = 'id asc', $limit = '') {
		return $this->where ( $where )->order ( $order )->limit ( $limit )->select ();
	}
	
	/**
	 * 根据条件获取当前地区的上级地区ID
	 * @param int $id
	 */
	public function getParentById($id) {
		$where ['id'] = $id;
		return $this->where ( $where )->getField ( 'parentid' );
	}
	
	/**
	 * 根据上级地区ID获取ID下所有的地区名
	 * @param int $parentid
	 */
	public function getNameByPid($parentid) {
		$where ['parentid'] = $parentid;
		return $this->where ( $where )->getField ( 'region_name', true );
	}
}