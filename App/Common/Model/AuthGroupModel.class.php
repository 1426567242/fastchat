<?php
/**
 * 用户角色模型类
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-5-28
 */
namespace Common\Model;
class AuthGroupModel extends \Think\Model {
	
	protected $_validate = array (
		array ('title', 'require', '角色名称不能为空' ), 
		array ('group_desc', '0,80', '角色描述最多80个字符', self::VALUE_VALIDATE, 'length', self::MODEL_BOTH ) 
	);
	protected $_auto = array (
		array ('module', 'Admin', self::MODEL_INSERT ), 
		array ('group_type', '1', self::MODEL_INSERT )
	);
	
	public function _initialize() {
	
	}
	
	/**
	 * 根据制定条件获取用户角色数量
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getCount($where = array(), $order = 'id asc') {
		return $this->where ( $where )->order ( $order )->count ();
	}
	
	/**
	 * 根据指定条件获取用户角色列表
	 * @param array $where
	 * @param string $order
	 * @param string $limit
	 */
	public function getGroup($where = array(), $order = 'id asc', $limit = '') {
		return $this->where ( $where )->order ( $order )->limit ( $limit )->select ();
	}
	
	/**
	 * 根据条件获取角色制定字段信息
	 * @param string $field
	 * @param array $where
	 */
	public function getGroupField($field, $where = array()) {
		return $this->where ( $where )->getField ( $field );
	}
	
	/**
	 * 根据角色ID获取角色组信息
	 * @param int $id
	 */
	public function getGroupById($id) {
		$where ['id'] = $id;
		return $this->where ( $where )->find ();
	}

}