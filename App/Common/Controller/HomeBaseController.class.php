<?php
/**
 * 前台基类控制器
 * ===============================================
 * @copyright 深圳市顶速网络科技有限公司  http://fasttop.top
 * ===============================================
 * @author: Ketity <970564173@qq.com>
 * @date: 2015-6-4
 */
namespace Common\Controller;
class HomeBaseController extends \Think\Controller {
	
	protected $_User = array ();
	
	/**
	 * 初始化
	 */
	public function _initialize() {
		$this->_User = $this->setAuthor (); //设置用户访问信息
		/* 读取数据库中的配置 */
		$config = S ( 'DB_CONFIG_DATA' );
		if (! $config) {
			$config = D ( 'Config' )->getList ();
			S ( 'DB_CONFIG_DATA', $config );
		}
		C ( $config ); // 添加配置
		if (! C ( 'WEB_SITE_CLOSE' )) { //判断是否开启了站点
			echo "<script>alert('站点暂未开放！')</script>";
			exit ();
		}
		//站点seo信息
		$seoInfo = array ('title' => C ( 'WEB_SITE_TITLE' ), 'description' => C ( 'WEB_SITE_DESCRIPTION' ), 'keyword' => C ( 'WEB_SITE_KEYWORD' ) );
		$this->assign ( 'seoInfo', $seoInfo );
	}
	
	/**
	 * 设置游客身份
	 */
	public function setVisitor() {
		$randString = \Org\Util\String::randString ();
		$userInfo ['user_online_id'] = substr ( time () . $randString, - 8 );
		$userInfo ['user_online_name'] = '游客' . $userInfo ['user_online_id'];
		$userInfo ['role'] = 0;
		$userInfo ['createtime'] = NOW_TIME;
		return $userInfo;
	}
	
	/**
	 * 设置用户访问信息，包括游客、会员
	 */
	public function setAuthor() {
		define ( 'CUID', D ( 'User' )->checkLogin () );
		if (! CUID) { //用户不存在， 以游客身份登录
			$userInfo = session ( 'userInfo' );
			if (empty ( $userInfo['user_online_id'] )) {
				$userInfo = $this->setVisitor ();
			}
		} else {
			$user = D ( 'User' )->getUser ( 'uid', CUID );
			$userInfo ['user_online_id'] = $user ['uid'];
			$userInfo ['user_online_name'] = $user ['username'];
			$userInfo ['role'] = 1;
			$userInfo ['createtime'] = NOW_TIME;
		}
		session ( 'userInfo', $userInfo );
		return $userInfo;
	}
}